#!/usr/bin/env python

# std python libraries
import numpy as np
import time

# vision libraries
import cv2 as cv
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import CompressedImage, Image

#custom libraries
from node_camera import *

class TestInterface(object):
    """docstring for TestInterface."""

    def __init__(self):
        super(TestInterface, self).__init__()
        # topic root
        self.vision = CameraTest()
        self.topic_root = 'MIRO_TEST'
        #'/' + os.getenv("MIRO_ROBOT_TEST") + '/'

        # state
#		self.m_ready = True

        #Arrays to hold image topics
#		self.cam_left_image = None
#		self.cam_right_image = None
        #Create object to convert ROS images to OpenCV format

        self.image_converter = CvBridge()

        #Subscribe to Camera topics
        self.cam_sub_l = rospy.Subscriber(self.topic_root + "/sensors/cam_l/", Image, self.callback_cam_l, tcp_nodelay=True)
        self.cam_sub_r = rospy.Subscriber(self.topic_root + "/sensors/cam_r/", Image, self.callback_cam_r, tcp_nodelay=True)

    def callback_cam_l(self, ros_image):
        self.vision.process(ros_image, 0)

    def callback_cam_r(self, ros_image):
        self.vision.process(ros_image, 1)
