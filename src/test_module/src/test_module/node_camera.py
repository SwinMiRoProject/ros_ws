#!/usr/bin/env python

# std python libraries
import numpy as np
import time


# vision libraries
import cv2 as cv
from cv_bridge import CvBridge, CvBridgeError

#ros libraries
import rospy
# from std_msgs.msg import Float32MultiArray, UInt32MultiArray, UInt16MultiArray, UInt8MultiArray, UInt16, Int16MultiArray, String
from sensor_msgs.msg import CompressedImage, Image

class CameraTest(object):
    """docstring for CameraTest."""

    def __init__(self):
        super(CameraTest, self).__init__()

        self.image_converter = CvBridge()

        self.cameras = [None, None]
        self.cam_images = [None, None]

        self.topic_root = 'MIRO_TEST' #'/' + os.getenv("MIRO_ROBOT_TEST") + '/'
        self.image_pub_l = rospy.Publisher(self.topic_root + "/sensors/cam_l/", Image, queue_size=0)
        self.image_pub_r = rospy.Publisher(self.topic_root + "/sensors/cam_r/", Image, queue_size=0)

        self.cam_ret = [None, None]

    def setup_cam(self):
        for index in range(0,2):
            cap = cv.VideoCapture(index)
            ret, img = cap.read()

            if ret is True:
                self.cameras[index] = cap
            else:
                self.cameras[index] = None

    def publish(self, index):
        if index == 0:
            self.image_pub_l.publish(self.image_converter.cv2_to_imgmsg(self.cam_images[0], "bgr8"))
        else:
            self.image_pub_r.publish(self.image_converter.cv2_to_imgmsg(self.cam_images[1], "bgr8"))
        #print('index ' + str(index) + ' published')

    def process(self, img_data, index):
        try:
            #cam_image = self.image_converter.compressed_imgmsg_to_cv2(img_data, "bgr8")
            cam_image = self.image_converter.imgmsg_to_cv2(img_data, "bgr8")
            self.cam_images[index] = cam_image
        except CvBridgeError as e:
            print("Conversion of image failed \n")
            print(e)

        return

    def get_image(self, index):
        if not self.cameras[index] is None:
            ret, cam = self.cameras[index].read()
            self.cam_images[index] = None

            if ret is True:
                self.cam_images[index] = cam

            self.cam_ret[index] = ret

    # def print_image(self, index):
    #     if not self.cam_images[index] is None:
    #         if index is 0:
    #             cv.imshow('img1', self.cam_images[0])
    #         else:
    #             cv.imshow('img2', self.cam_images[1])
    #         cv.waitKey(1)

    # def loop(self):
    #     rate = rospy.Rate(10) # 10Hz
    #     while not rospy.is_shutdown():
    #         for index in range(0,2):
    #             self.get_image(index)
    #         for index in range(0,2):
    #             if not self.cam_images[index] is None:
    #                 #self.print_image(index)
    #                 self.publish(index)
    #
    #         if (self.cam_ret[0] is False or self.cam_ret[0] is False) and (self.cam_ret[1] is False or  self.cam_ret[0] is None):
    #             print('connection to all cameras lost, terminating program')
    #             break
    #
    #         rate.sleep()
