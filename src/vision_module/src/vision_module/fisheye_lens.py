#!/usr/bin/python

#standard python libraries
import numpy as np
import glob

#vision libraries
import cv2 as cv



from test_module.interface_camera import TestInterface

class VisionCorrection(object):
    """docstring for VisionCorrection."""

    def __init__(self):
        super(VisionCorrection, self).__init__()
        self.interface = TestInterface()
        self.fisheye = cv.fisheye()

        self.calibrate[None, None, None]
        self.images[None, None]

    def calibrate(image, index):
        self.images[index] = self.interface.vision.cam_images[index]

    def correct_image(image):

    def show_image(image_name, image):

    def quit_loop():

    def loop():
