#!/usr/bin/env python

from test_module.node_camera import CameraTest
import rospy
import cv2 as cv

class PublisherTest(object):
    """docstring for PublisherTest."""

    def __init__(self):
        super(PublisherTest, self).__init__()
        self.cam_node = CameraTest()

    def print_image(self, index, frame_name):
        if not self.cam_node.cam_images[index] is None:
                cv.imshow(frame_name, self.cam_node.cam_images[index])
        else:
            print('no image found in image array: '+str(index))
        cv.waitKey(1)

    def loop(self):
        rate = rospy.Rate(10) # 10Hz
        while not rospy.is_shutdown():
            for index in range(0,2):
                self.cam_node.get_image(index)
            for index in range(0,2):
                if not self.cam_node.cam_images[index] is None:
                    self.print_image(index, 'publisher - camera ' + str(index))
                    self.cam_node.publish(index)

            if (self.cam_node.cameras[0] is None or False) and (self.cam_node.cameras[1] is None or False):
                print('connection to cameras lost, terminating program')
                break

            if cv.waitKey(1) & 0xFF == ord('q'):
                break

            rate.sleep()

def main():
    rospy.init_node("test_publisher_camera", anonymous=False)
    pub_test = PublisherTest()
    pub_test.cam_node.setup_cam()
    if (pub_test.cam_node.cameras[0] is None) and (pub_test.cam_node.cameras[1] is None):
        print('no cameras detected')
    else:
        pub_test.loop()

if __name__ == '__main__':
    main()
