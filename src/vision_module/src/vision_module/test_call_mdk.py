import numpy as np

import miro2 as miro
import copy as cp

class testMiRo(object):
    """docstring for testMiRo."""

    def __init__(self):
        super(testMiRo, self).__init__()
        self.cam_model = miro.utils.CameraModel()
        self.cam_images = [None, None]

    def insert_images(image_array=None):
        for index, image in enumerate(image_array):
            if not image is None:
                self.cam_images[index] = image

    def remove_fisheye_lens(image, index):
        self.cam_images[index] = cp.copy(image)

    def add_fisheye_lens(image, index):
        self.cam_images[index] = cp.copy(image)
