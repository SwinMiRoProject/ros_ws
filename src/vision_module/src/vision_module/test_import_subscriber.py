#!/usr/bin/env python

from test_module.interface_camera import TestInterface
#from test_module.node_camera import CameraTest
import rospy
import cv2 as cv

class FramesTest(object):
    """docstring for FramesTest."""

    def __init__(self):
        super(FramesTest, self).__init__()
        self.interface = TestInterface()
        self.cam_imgs = [None, None]

    def print_image(self, index, frame_name):
        if not self.cam_imgs[index] is None:
                cv.imshow(frame_name, self.cam_imgs[index])
        else:
            print('no image found in image array: '+str(index))

    def loop(self):
        while not rospy.is_shutdown():
            for i in range(0,2):
                self.cam_imgs[i] = self.interface.vision.cam_images[i]

            for i in range(0,2):
                self.print_image(i, 'subscriber - camera ' + str(i))
            #cv.imshow('img1', self.cam_imgs[0])
            if cv.waitKey(1) & 0xFF == ord('q'):
                break

            for i in range(0,2):
                self.cam_imgs[i] = None

if __name__ == '__main__':
    rospy.init_node("test_subscriber_camera", anonymous=False)
    main = FramesTest()
    main.loop()
